from random import randint, sample

from championship.constants import DEFENSE_ARRAY
from championship.models import Player


def create_games(players):
    """
    Draws matches for players and
    return pairs of player to be played with game id
    :players list of player in an array
    and assuming players length will be even numbers
    """
    games = []
    game = []

    if not len(players) % 2 == 0:
        player = players.pop()
        games.append([player])

    for index, player in enumerate(players):
        game.append(player)
        if len(game) == 2:
            games.append(game)
            game = []

    return games


def play(player1, player2):
    """
    if player 1 is offensive: 
        check if random number is not in defense array of player2 then player1 gets the points
        else player2 get the points and change playing style to defensive
    else player1 is defensive, 
        check if random number is not in defense array of player1, then player2 gets the points
        else player1 get the points and chane playing style to defensive
    """
    player1_length = Player.objects.get(
        id=player1['player']).defence_set_length
    player2_length = Player.objects.get(
        id=player2['player']).defence_set_length
    if player1['style'] == 'offensive':
        if randint(1, 10) not in sample(DEFENSE_ARRAY, player2_length):
            player1['score'] += 1
        else:
            player2['score'] += 1
            player1['style'] = 'offensive'
            player2['style'] = 'defensive'
    else:
        if randint(1, 10) not in sample(DEFENSE_ARRAY, player1_length):
            player2['score'] += 1
        else:
            player1['score'] += 1
            player1['style'] = 'offensive'
            player2['style'] = 'defensive'


def export_championship(referee):
    """
    For representation purpose only
    """
    print '======================'
    print 'Championship', referee.id
    print 'Champion:', referee.champion.name
    print 'Players: {}'.format(
        ', '.join(referee.players.values_list('name', flat=True)))
    print '======================'
    print 'Game Details'
    for game in referee.games.all():
        print '======================'
        print 'Game ID: {}'.format(game.id)
        player_name = game.player.name
        opponent_name = game.opponent.name if game.opponent else None
        if not game.opponent:
            print 'A bye is given to', player_name
        else:
            print 'The Game between {} and {}'.format(
                player_name,
                opponent_name)
            print 'Scores for {}({}) and {}({})'.format(
                player_name,
                game.player_score,
                opponent_name,
                game.opponent_score)
        print 'The Winner is', game.winner.name
