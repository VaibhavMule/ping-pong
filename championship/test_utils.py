import json

from django.test import Client

all_games = []


def start_games(players):
    c = Client()
    results = []

    response = c.post('/referee/draw_games/', {
        'players': players})
    # self.assertIs(response.status_code, 200)
    games = response.json()['games']

    for game in games:
        # draw a game
        response_games = c.post('/referee/games/', {
            'player': game[0],
            'opponent': game[1] if len(game) == 2 else ''
        })
        # self.assertIs(response_games.status_code, 201)
        game_json = response_games.json()

        all_games.append(game_json['id'])

        # play game and get results
        payload = {
            'id': game_json['id'],
            'player': game_json['player'],
            'opponent': game_json['opponent']
            if game_json['opponent'] else ''
        }
        response_play_game = c.post('/players/play_game/', payload)
        game_id = str(response_play_game.json()['id'])

        # update game results
        response_game = c.patch('/referee/games/' + game_id + '/',
                                json.dumps(response_play_game.json()),
                                content_type='application/json')
        # print response_game.content
        results.append(response_game.json())

    if len(results) == 1:
        return results[0], all_games
    else:
        return start_games([int(player['winner']) for player in results])
