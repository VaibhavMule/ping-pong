# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json

from django.test import TestCase, Client

from championship.models import Player
from championship.test_utils import start_games
from championship.constants import PLAYER


class Championship(TestCase):

    def test_if_championship_is_getting_started(self):
        """
        check if player are able to join
        """
        c = Client()
        # Register all players
        for player in PLAYER:
            response = c.post('/players/', player)
            # print response.content
            self.assertIs(response.status_code, 201)

        # Refree start champianship and start games.
        players = Player.objects.values_list('id', flat=True)
        response_referee = c.post('/referee/', {
            'players': players})
        referee_id = str(response_referee.json()['id'])
        champion, games = start_games(players)
        # print champion, games

        # update refree championship
        url = '/referee/{}/'.format(referee_id)
        payload = json.dumps({
            "champion": champion['winner'],
            "games": games})
        response = c.patch(url, payload, content_type='application/json')
        # print response.content

        # Export to terminal
        export = c.get(url + 'export/')
        # print export.content, export.status_code
