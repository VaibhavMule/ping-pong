# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from random import shuffle

from rest_framework.decorators import api_view, detail_route
from rest_framework.response import Response
from rest_framework import viewsets

from championship.serializers import (
    PlayerSerializer,
    RefereeSerializer,
    GameSerializer,
    DrawGameSerializer,
    PlayGameSerializer)
from championship.models import Player, Referee, Game
from championship.utils import play, create_games, export_championship


@api_view(['POST'])
def draw_games(request):
    """
    Draw games
    players: list of players ids
    """
    serializer = DrawGameSerializer(data=request.data)
    if serializer.is_valid():
        players = serializer.data['players']
        shuffle(players)
        return Response({
            'message': 'Game Drawn successfully',
            'games': create_games(players)})
    else:
        return Response(serializer.errors, status=400)


@api_view(['POST'])
def play_game(request):
    """
    players play match and return results.
    :game game id
    :return Match results
    """
    serializer = PlayGameSerializer(data=request.data)
    if serializer.is_valid():
        data = serializer.data

        if not data['opponent']:
            # bye the player
            return Response({
                'id': data['id'],
                'player_score': 5,
                'winner': data['player']})
        else:
            player1 = {
                'player': data['player'],
                'score': 0,
                'style': 'offensive'}
            player2 = {
                'player': data['opponent'],
                'score': 0,
                'style': 'defensive'}

            while (player1['score'] < 5 and player2['score'] < 5):
                play(player1, player2)

            winner = player1['player']  \
                if player1['score'] == 5 else player2['player']

            return Response({
                'id': data['id'],
                'player_score': player1['score'],
                'opponent_score': player2['score'],
                'winner': winner,
            })
    else:
        return Response(serializer.errors, status=400)


class PlayerViewSet(viewsets.ModelViewSet):
    """
    Player CRUD API
    """
    queryset = Player.objects.all()
    serializer_class = PlayerSerializer


class RefereeViewSet(viewsets.ModelViewSet):
    """
    Championship CRUD API
    """
    queryset = Referee.objects.all()
    serializer_class = RefereeSerializer

    @detail_route()
    def export(self, request, *args, **kwargs):
        """
        Just printing for now
        """
        referee = self.get_object()
        export_championship(referee)
        return Response(referee.id)


class GameViewSet(viewsets.ModelViewSet):
    """
    Championship CRUD API
    """
    queryset = Game.objects.all()
    serializer_class = GameSerializer
