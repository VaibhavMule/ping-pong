from rest_framework import serializers

from championship.models import Player, Referee, Game


class PlayerSerializer(serializers.ModelSerializer):

    def validate_defence_set_length(self, length):
        if length >= 1 and length <= 10:
            return length
        else:
            raise serializers.ValidationError(
                'Defence set length should be beetween 1 to 10.')

    class Meta:
        model = Player
        fields = '__all__'


class RefereeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Referee
        fields = '__all__'
        extra_kwargs = {
            'games': {'allow_empty': True}}


class GameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Game
        fields = '__all__'


class DrawGameSerializer(serializers.Serializer):
    players = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Player.objects.all())


class PlayGameSerializer(serializers.ModelSerializer):

    class Meta:
        model = Game
        fields = ('player', 'opponent', 'id',)
        extra_kwargs = {'id': {'read_only': False}}
