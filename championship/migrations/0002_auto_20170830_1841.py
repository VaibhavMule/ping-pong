# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-30 18:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('championship', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='referee',
            name='champion',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='champion', to='championship.Player'),
        ),
    ]
