
from championship import views
from rest_framework import routers

from django.conf.urls import url

urlpatterns = [
    url(r'^referee/draw_games/', views.draw_games),
    url(r'^players/play_game/', views.play_game)
]

router = routers.DefaultRouter()
router.register(r'^players', views.PlayerViewSet)
router.register(r'^referee/games', views.GameViewSet)
router.register(r'^referee', views.RefereeViewSet)
urlpatterns += router.urls
