# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Player(models.Model):
    name = models.CharField(max_length=25)
    defence_set_length = models.PositiveSmallIntegerField()
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class Game(models.Model):
    player = models.ForeignKey(Player, related_name="player")
    opponent = models.ForeignKey(Player, related_name="opponent", null=True)
    player_score = models.PositiveSmallIntegerField(default=0)
    opponent_score = models.PositiveSmallIntegerField(default=0)
    winner = models.ForeignKey(Player, related_name="winner", null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '{} vs {}'.format(self.player.name, self.opponent.name) \
            if self.opponent else 'bye {}'.format(self.player.name)


class Referee(models.Model):
    games = models.ManyToManyField(Game)
    players = models.ManyToManyField(Player)
    champion = models.ForeignKey(Player, related_name='champion', null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return 'Championship {}'.format(self.id)
