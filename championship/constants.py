PLAYER = [
    {
        'name': 'Nikhil',
        'defence_set_length': 8},
    {
        'name': 'Mihir',
        'defence_set_length': 8},
    {
        'name': 'Amit',
        'defence_set_length': 7},
    {
        'name': 'Joey',
        'defence_set_length': 7},
    {
        'name': 'Vivek',
        'defence_set_length': 6},
    {
        'name': 'Russel',
        'defence_set_length': 5},
    {
        'name': 'Pritam',
        'defence_set_length': 5
    },
]

DEFENSE_ARRAY = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
