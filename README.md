# Ping Pong

## APIs

	URL: /players/
	description: This is CRUD API for player resource to store name, defense set length of player.

	URL: /referee/
	description: This is CRUD API for referee resource to store players, games, champion. Whenever refree start champioanship, it will be stored in referee.

	URL: /referee/:id/export/
	description: Exenttion to CRUD API to export referee report for the champianship.

	URL: /referee/games
	description: This is CRUD API for game resource to store player, opponent, their score and winner. 

	URL: referee/draw_games/
	methods: Allow only POST requests
	description: This API draws games with given players list.

	URL: players/play_game/
	methods: Allow only POST requests
	description: it start game with given game id and give response of winner, and player's score.


## Installation Instructions
Make sure Python 2.7, pip and virtualenv installed.

- Install PIP and virtualenv

```
$ sudo apt-get install python-pip
$ pip install virtualenv
```

- go to project root direcotry and run following command to create and activate virtual environment.
```
$ virtualenv venv
$ source venv/bin/activate
```

- Install dependacy and run server.
```
$ pip install -r requirements.txt
$ ./manage.py runserver
```

- Run command to simulate the game in other tab of termial.
```
$ ./manage.py test championship
```

You should get following output
			
		======================
		Championship 1
		Champion: Mihir
		Players: Nikhil, Mihir, Amit, Joey, Vivek, Russel, Pritam
		======================
		Game Details
		======================
		Game ID: 1
		A bye is given to Russel
		The Winner is Russel
		======================
		Game ID: 2
		The Game between Vivek and Nikhil
		Scores for Vivek(1) and Nikhil(5)
		The Winner is Nikhil
		======================
		Game ID: 3
		The Game between Amit and Mihir
		Scores for Amit(0) and Mihir(5)
		The Winner is Mihir
		======================
		Game ID: 4
		The Game between Pritam and Joey
		Scores for Pritam(3) and Joey(5)
		The Winner is Joey
		======================
		Game ID: 5
		The Game between Russel and Nikhil
		Scores for Russel(5) and Nikhil(4)
		The Winner is Russel
		======================
		Game ID: 6
		The Game between Joey and Mihir
		Scores for Joey(0) and Mihir(5)
		The Winner is Mihir
		======================
		Game ID: 7
		The Game between Russel and Mihir
		Scores for Russel(1) and Mihir(5)
		The Winner is Mihir



